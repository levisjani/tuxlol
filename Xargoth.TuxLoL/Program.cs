﻿// Copyright (c) 2013 The TuxLoL authors.
// 
// This file is part of TuxLoL.
// 
// TuxLoL is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// TuxLoL is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with TuxLoL.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CommandLine;
using Xargoth.TuxLoL.Core;
using Xargoth.TuxLoL.Core.Utility;
using Xargoth.TuxLoL.Properties;

namespace Xargoth.TuxLoL
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            ParserResult<object> result = Parser.Default.ParseArguments(args, typeof (PatchOptions),
                                                                        typeof (RestoreOptions));
            if (result.Errors.Any())
            {
                return;
            }

            var patchOptions = result.Value as PatchOptions;
            if (patchOptions != null)
            {
                Patch(patchOptions);
                return;
            }

            var restoreOptions = result.Value as RestoreOptions;
            if (restoreOptions != null)
            {
                Restore(restoreOptions);
            }
        }

        private static void Patch(PatchOptions patchOptions)
        {
            patchOptions.LoLDirectory = SetLoLDirectory(patchOptions.LoLDirectory);
            if (!ValidatePatchOptions(patchOptions))
            {
                return;
            }

            try
            {
                ApplyPatch(patchOptions);
                Console.WriteLine("Patch successfully applied!");
            }

            catch (IOException)
            {
                Console.WriteLine("Unable to access the League of Legends directory. " +
                                  "Make sure that League of Legends is closed.");
            }

            catch (AggregateException)
            {
                Console.WriteLine("Failed to apply the patch.");
            }
        }

        private static bool ValidatePatchOptions(PatchOptions patchOptions)
        {
            if (!Directory.Exists(patchOptions.LoLDirectory))
            {
                Console.WriteLine("The specified directory is invalid.");
                return false;
            }

            return true;
        }

        private static void ApplyPatch(PatchOptions options)
        {
            string[] paths = Settings.Default.Paths.Split(';');
            foreach (string directory in Directory.GetDirectories(options.LoLDirectory))
            {
                Parallel.ForEach(Directory.GetFiles(directory, "*.raf"), fileName =>
                {
                    RiotArchiveFile archive = RiotArchiveReader.Read(fileName);

                    bool hasPatched = false;
                    foreach (var file in archive.FileList)
                    {
                        RiotArchiveFileListEntry entry = file;
                        if (Path.GetExtension(entry.Path) != ".dds" || !paths.Contains(x => entry.Path.StartsWith(x)))
                        {
                            continue;
                        }

                        byte[] decompressedData = ZLibHelper.Decompress(entry.Data);
                        using (var patcher = new TexturePatcher(decompressedData))
                        {
                            if (patcher.CanPatch())
                            {
                                patcher.Patch();
                                hasPatched = true;
                            }
                        }

                        file.Data = ZLibHelper.Compress(decompressedData);
                    }

                    if (!hasPatched)
                    {
                        return;
                    }

                    if (!options.NoBackup)
                    {
                        File.Copy(fileName, fileName + ".bak", true);
                        File.Copy(fileName + ".dat", fileName + ".dat.bak", true);
                    }

                    RiotArchiveWriter.Write(archive, fileName);
                    Console.WriteLine("Patched {0}!", Path.GetFileName(fileName));
                });
            }
        }

        private static void Restore(RestoreOptions restoreOptions)
        {
            restoreOptions.LoLDirectory = SetLoLDirectory(restoreOptions.LoLDirectory);
            if (!Directory.Exists(restoreOptions.LoLDirectory))
            {
                Console.WriteLine("The specified directory is invalid.");
                return;
            }

            try
            {
                RestoreBackup(restoreOptions.LoLDirectory);
                Console.WriteLine("Restore completed successfully.");
            }

            catch (IOException)
            {
                Console.WriteLine("Unable to access the League of Legends directory. " +
                                  "Make sure that League of Legends is closed.");
            }

            catch (AggregateException)
            {
                Console.WriteLine("Failed to revert the changes made to League of Legends.");
            }
        }

        private static void RestoreBackup(string loLDirectory)
        {
            foreach (string directory in Directory.GetDirectories(loLDirectory))
            {
                Parallel.ForEach(Directory.GetFiles(directory, "*.raf"), fileName =>
                {
                    string backupRaf = fileName + ".bak";
                    string backupDat = fileName + ".dat.bak";
                    if (File.Exists(backupRaf) && File.Exists(backupDat))
                    {
                        File.Copy(backupRaf, fileName, true);
                        File.Copy(backupDat, fileName + ".dat", true);
                        Console.WriteLine("Restored {0}.", Path.GetFileName(fileName));
                        File.Delete(fileName + ".bak");
                        File.Delete(fileName + ".dat.bak");
                    }
                });
            }
        }

        private static string SetLoLDirectory(string loLDirectory)
        {
            return Path.Combine(loLDirectory, "RADS", "projects", "lol_game_client", "filearchives");
        }
    }
}