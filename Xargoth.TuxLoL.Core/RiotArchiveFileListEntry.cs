﻿// Copyright (c) 2013 The TuxLoL authors.
// 
// This file is part of TuxLoL.
// 
// TuxLoL is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// TuxLoL is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with TuxLoL.  If not, see <http://www.gnu.org/licenses/>.

using System;

namespace Xargoth.TuxLoL.Core
{
    public class RiotArchiveFileListEntry
    {
        private byte[] data;
        private string path;

        public RiotArchiveFileListEntry(string path, byte[] data)
        {
            if (path == null)
            {
                throw new ArgumentNullException("path");
            }

            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            this.path = path;
            this.data = data;
        }

        public bool IsCompressed
        {
            get { return RiotArchiveHelper.IsCompressed(Path); }
        }

        public string Path
        {
            get { return path; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }

                path = value;
            }
        }

        public byte[] Data
        {
            get { return data; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }

                data = value;
            }
        }

        internal int DataOffset { get; set; }

        internal int DataSize { get; set; }
    }
}