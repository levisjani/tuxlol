﻿// Copyright (c) 2013 The TuxLoL authors.
// 
// This file is part of TuxLoL.
// 
// TuxLoL is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// TuxLoL is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with TuxLoL.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xargoth.TuxLoL.Core.Utility;

namespace Xargoth.TuxLoL.Core
{
    public class RiotArchiveReader
    {
        private readonly BinaryReader archiveReader;
        private readonly FileStream archiveStream;
        private readonly FileStream dataStream;

        private RiotArchiveReader(string fileName)
        {
            if (fileName == null)
            {
                throw new ArgumentNullException("fileName");
            }

            if (!File.Exists(fileName))
            {
                throw new FileNotFoundException();
            }

            archiveStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            archiveReader = new BinaryReader(archiveStream);
            if (archiveReader.ReadInt32() != RiotArchiveFile.MagicNumber)
            {
                throw new ArgumentException("Archive file broken", "fileName");
            }

            dataStream = new FileStream(fileName + ".dat", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
        }

        private RiotArchiveFile Read()
        {
            int version = archiveReader.ReadInt32();
            int managerIndex = archiveReader.ReadInt32();
            int fileListOffset = archiveReader.ReadInt32();
            int pathListOffset = archiveReader.ReadInt32();
            string[] pathList = ReadPathList(pathListOffset).ToArray();
            List<RiotArchiveFileListEntry> fileList = ReadFileList(fileListOffset, pathList);
            archiveReader.Close();
            return new RiotArchiveFile(version, managerIndex, fileList);
        }

        private IEnumerable<string> ReadPathList(int pathListOffset)
        {
            archiveStream.Position = pathListOffset;
            archiveReader.ReadInt32(); // path list size in bytes
            // An entry only contains an offset and the length of the path, so it is not needed to read these entries
            int entries = archiveReader.ReadInt32();
            archiveReader.ReadBytes(entries * 8); // 1 entry ≙ 8 bytes

            for (int i = 0; i < entries; i++)
            {
                yield return archiveReader.ReadNullTerminatedString();
            }
        }

        private List<RiotArchiveFileListEntry> ReadFileList(int fileListOffset, string[] pathList)
        {
            archiveStream.Position = fileListOffset;
            int entries = archiveReader.ReadInt32();
            var fileList = new List<RiotArchiveFileListEntry>(entries);

            for (int i = 0; i < entries; i++)
            {
                archiveReader.ReadUInt32(); // Hash not needed
                int dataOffset = archiveReader.ReadInt32();
                int dataSize = archiveReader.ReadInt32();
                int pathListIndex = archiveReader.ReadInt32();
                string path = pathList[pathListIndex];
                var file = new RiotArchiveFileListEntry(path, GetData(dataOffset, dataSize));
                fileList.Add(file);
            }

            return fileList;
        }

        private byte[] GetData(int offset, int size)
        {
            using (var stream = new MemoryStream())
            {
                dataStream.CopySection(stream, offset,
                                       size);
                return stream.ToArray();
            }
        }

        public static RiotArchiveFile Read(string fileName)
        {
            var reader = new RiotArchiveReader(fileName);
            return reader.Read();
        }
    }
}