﻿// Copyright (c) 2013 The TuxLoL authors.
// 
// This file is part of TuxLoL.
// 
// TuxLoL is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// TuxLoL is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with TuxLoL.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.IO.Compression;
using ICSharpCode.SharpZipLib.Checksums;

namespace Xargoth.TuxLoL.Core.Utility
{
    public static class ZLibHelper
    {
        public static byte[] Compress(byte[] data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            using (var output = new MemoryStream())
            {
                // ZLib Header 0x78 0x9C
                output.WriteByte(0x78);
                output.WriteByte(0x9C);
                using (var input = new MemoryStream(data))
                {
                    using (var compressionStream = new DeflateStream(output, CompressionMode.Compress, true))
                    {
                        input.CopyTo(compressionStream);
                        compressionStream.Close();

                        // Adler32 hash of the uncompressed data
                        var adler32 = new Adler32();
                        adler32.Update(data);
                        byte[] hash = BitConverter.GetBytes((int) adler32.Value);
                        Array.Reverse(hash);
                        output.Write(hash, 0, hash.Length);
                        return output.ToArray();
                    }
                }
            }
        }

        public static byte[] Decompress(byte[] data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            var compressedData = new byte[data.Length - 2 - 4]; // Header + Adler32
            Buffer.BlockCopy(data, 2, compressedData, 0, compressedData.Length);

            using (var input = new MemoryStream(compressedData))
            {
                using (var output = new MemoryStream())
                {
                    using (var compressionStream = new DeflateStream(input, CompressionMode.Decompress))
                    {
                        compressionStream.CopyTo(output);
                        return output.ToArray();
                    }
                }
            }
        }
    }
}