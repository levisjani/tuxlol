﻿// Copyright (c) 2013 The TuxLoL authors.
// 
// This file is part of TuxLoL.
// 
// TuxLoL is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// TuxLoL is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with TuxLoL.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xargoth.TuxLoL.Core.Utility;

namespace Xargoth.TuxLoL.Core
{
    public class RiotArchiveWriter
    {
        private readonly RiotArchiveFile archive;
        private readonly FileStream archiveStream;
        private readonly BinaryWriter archiveWriter;
        private readonly FileStream dataStream;

        private RiotArchiveWriter(RiotArchiveFile archive, string fileName)
        {
            if (archive == null)
            {
                throw new ArgumentNullException("archive");
            }

            if (fileName == null)
            {
                throw new ArgumentNullException("fileName");
            }

            this.archive = archive;
            archiveStream = new FileStream(fileName, FileMode.Create);
            archiveWriter = new BinaryWriter(archiveStream);
            dataStream = new FileStream(fileName + ".dat", FileMode.Create);
        }

        private void Write()
        {
            using (archiveWriter)
            {
                archiveWriter.Write(RiotArchiveFile.MagicNumber);
                archiveWriter.Write(archive.Version);
                archiveWriter.Write(archive.ManagerIndex);

                const int fileListOffset = 20;
                int pathListOffset = fileListOffset + archive.FileList.Count * 16 + 4;
                archiveWriter.Write(fileListOffset);
                archiveWriter.Write(pathListOffset);

                List<string> pathList = archive.FileList.Select(file => file.Path).ToList();
                WriteFileList(pathList);
                WritePathList(pathList);
                WriteDataFile();
            }
        }

        private void WriteFileList(IList<string> pathList)
        {
            archiveWriter.Write(archive.FileList.Count);

            int current = 0;
            foreach (var file in archive.FileList)
            {
                file.DataOffset = current;
                file.DataSize = file.Data.Length;
                current = current + file.DataSize;

                archiveWriter.Write(RiotArchiveHash.GetHash(file.Path));
                archiveWriter.Write(file.DataOffset);
                archiveWriter.Write(file.DataSize);
                archiveWriter.Write(pathList.IndexOf(file.Path));
            }
        }

        private void WritePathList(ICollection<string> pathList)
        {
            int pathSize = pathList.Sum(path => path.Length + 1); // + 1, as all strings are null terminated
            int pathListSize = 8 + 8 * pathList.Count + pathSize;
            archiveWriter.Write(pathListSize);
            archiveWriter.Write(pathList.Count);

            int current = 8 + pathList.Count * 8;
            foreach (var path in pathList)
            {
                archiveWriter.Write(current);
                archiveWriter.Write(path.Length + 1);
                current += path.Length + 1; // +1, as all strings are null terminated
            }

            foreach (var path in pathList)
            {
                archiveWriter.WriteNullTerminatedString(path);
            }
        }

        private void WriteDataFile()
        {
            dataStream.SetLength(archive.FileList.Sum(file => file.DataSize));
            foreach (RiotArchiveFileListEntry file in archive.FileList)
            {
                dataStream.Position = file.DataOffset;
                dataStream.Write(file.Data, 0, file.Data.Length);
            }

            dataStream.Close();
        }

        public static void Write(RiotArchiveFile archive, string fileName)
        {
            if (archive == null)
            {
                throw new ArgumentNullException("archive");
            }

            if (fileName == null)
            {
                throw new ArgumentNullException("fileName");
            }

            var writer = new RiotArchiveWriter(archive, fileName);
            writer.Write();
        }
    }
}